import Phaser from 'phaser'

class Ground8 extends Phaser.Physics.Arcade.Sprite {
  constructor (scene, x, y) {
    super(scene, x, y, 'ground_8');
    scene.add.existing(this);
    scene.physics.world.enable(this);
  }

  update () {
  }
}

export {Ground8};