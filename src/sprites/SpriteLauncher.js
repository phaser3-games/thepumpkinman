class SpriteLauncher{
    constructor(scene, positionArray, group){
        this.scene=scene;
        this.positionArray=positionArray;
        this.group=group;
        this.positionArray.sort((a,b)=>{
            return a.x-b.x;
        });
    }

    createSprite(position){
        return null;
    }

    add2Group(sprite){
        return;
    }

    update(){
        for(let position of this.positionArray){
            position.x=position.x-1;
        }
        if(this.positionArray.length>0 && this.positionArray[0].x<400){
            let position=this.positionArray[0];
            this.positionArray.splice(0, 1);
            //launch the sprite
            let sprite=this.createSprite(position);
            if(sprite){
                this.add2Group(sprite);
            }
        }
    }
}

export {SpriteLauncher};