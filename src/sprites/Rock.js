import Phaser from 'phaser'

class Rock extends Phaser.Physics.Arcade.Sprite {
  constructor (scene, x, y) {
    super(scene, x, y, 'rock');
    scene.add.existing(this);
    scene.physics.world.enable(this);
  }

  update () {
  }
}

export {Rock};