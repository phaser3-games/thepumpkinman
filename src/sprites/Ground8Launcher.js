import {Ground8} from './Ground8'
import {SpriteLauncher} from './SpriteLauncher'
class Ground8Launcher extends SpriteLauncher{
    constructor(scene, positionArray, group){
        super(scene, positionArray, group);
    }

    createSprite(position){
        let ground8 = new Ground8(this.scene, position.x, position.y);
        return ground8;
    }

    add2Group(ground8){
        this.group.add(ground8);
    }
}

export {Ground8Launcher};