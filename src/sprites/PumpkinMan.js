import Phaser from 'phaser'

class PumpkinMan extends Phaser.Physics.Arcade.Sprite {
  constructor (scene, x, y) {
    super(scene, x, y, 'pumpkinman');
    scene.add.existing(this);
    scene.physics.world.enable(this);
    this.life=3;
  }

  getLife(){
    return this.life;
  }

  setLife(life){
    this.life=life;
  }

  update () {
  }
}

export {PumpkinMan};