import Phaser from 'phaser'
import pumpkinman from 'images/pumpkinman.png'
import spritesheet from 'images/spritesheet.png'
import ground_32 from 'images/ground_32.png'
import ground_8 from 'images/ground_8.png'
import rock from 'images/rock.png'
import bonus_life from 'images/bonus_life.png'
import cave from 'images/cave.jpg'

class Boot extends Phaser.Scene {
  constructor () {
    super({ key: 'Boot' })
  }

  preload () {
    const progress = this.add.graphics()

    this.load.on('fileprogress', (file, value) => {
      progress.clear()
      progress.fillStyle(0xffffff, 0.75)
      progress.fillRect(700 - (value * 600), 250, value * 600, 100)
    })

    this.load.image('pumpkinman', pumpkinman);
    this.load.image('ground_32', ground_32);
    this.load.image('ground_8', ground_8);
    this.load.image('rock', rock);
    this.load.image('bonus_life', bonus_life);
    this.load.spritesheet('pumpkinman_anim', 
        spritesheet,
        { frameWidth: 87, frameHeight: 115 }
    );
    this.load.image('cave', cave);
  }

  create () {
    this.scene.start('Game')
  }
}

export {Boot};