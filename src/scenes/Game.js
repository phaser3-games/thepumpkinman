import Phaser from 'phaser'

import { PumpkinMan } from '../sprites/PumpkinMan'
import { Ground8 } from '../sprites/Ground8'
import { Rock } from '../sprites/Rock'
import { RockLauncher } from '../sprites/RockLauncher'
import {Ground8Launcher} from '../sprites/Ground8Launcher'
import {LifeBonusLauncher} from '../sprites/LifeBonusLauncher'

class Game extends Phaser.Scene {
  constructor() {
    super({ key: 'Game' })
  }

  create() {
    this.startTime=new Date().getTime();
    this.tileSprite = this.add.tileSprite(400, 300, 1067, 600, "cave");
    this.platforms = this.physics.add.staticGroup();
    let g=this.platforms.create(400, 580, 'ground_32');
    g.setBounce(1);

    this.platforms2 = this.physics.add.group({
      allowGravity: false,
      immovable: true
    });

    let ground8Positions=[];
    let startX=200;
    for(let i=0; i<100; i++){
      let x=startX+(Math.random()*100%5)*100;
      startX=x;
      ground8Positions.push({
        x: x,
        y:400
      });
    }

    startX=200;
    for(let i=0; i<100; i++){
      let x=startX+(Math.random()*100%5)*100;
      startX=x;
      ground8Positions.push({
        x: x,
        y:200
      });
    }
    
    this.ground8Launcher=new Ground8Launcher(this, ground8Positions, this.platforms2);

    this.rocks = this.physics.add.group();
    let rockPositions=[];
    for(let i=0; i<50; i++){
      rockPositions.push({
        x: Math.random()*100000%10000+400,
        y:100
      });
    }
   
    this.rockLauncher=new RockLauncher(this, rockPositions, this.rocks);

    let lifeBonusPositions=[];
    for(let i=0; i<20; i++){
      lifeBonusPositions.push({
        x: Math.random()*100000%10000+400,
        y:100
      });
    }

    this.lifeBonuses = this.physics.add.group();
    this.lifeBonusLauncher=new LifeBonusLauncher(this, lifeBonusPositions, this.lifeBonuses);

    this.pumpkinman = new PumpkinMan(this, 44, 300);
    this.pumpkinman.body.setCollideWorldBounds(true);
    this.pumpkinman.body.setGravityY(300);

    this.lifeText=this.add.text(550, 10, "Life: "+this.pumpkinman.getLife(), {fontSize: '32px', fill: 'red'});
    this.timeText=this.add.text(10, 10, "0", {fontSize: '32px', fill: 'blue'});

    this.physics.add.collider(this.pumpkinman, this.platforms);
    this.physics.add.collider(this.pumpkinman, this.platforms2);
    this.physics.add.collider(this.rocks, this.platforms2);
    this.physics.add.collider(this.rocks, this.platforms);
    this.physics.add.overlap(this.pumpkinman, this.rocks, this.takeHit, null, this);
    this.physics.add.collider(this.lifeBonuses, this.platforms);
    this.physics.add.collider(this.lifeBonuses, this.platforms2);
    this.physics.add.overlap(this.pumpkinman, this.lifeBonuses, this.takeLifeBonus, null, this);

    this.anims.create({
      key: 'run',
      frames: this.anims.generateFrameNumbers('pumpkinman_anim', { start: 0, end: 9 }),
      frameRate: 25,
      repeat: -1
    });

    this.anims.create({
      key: 'jump',
      frames: this.anims.generateFrameNumbers('pumpkinman_anim', { start: 10, end: 17 }),
      frameRate: 2,
      repeat: -1
    });

    this.cursors = this.input.keyboard.createCursorKeys();
  }

  takeHit(obj1, obj2){
    let player=obj1;
    let rock=obj2;
    rock.disableBody(true, true);
    player.setLife(player.getLife()-1);
    if(player.getLife()<=0){
      player.disableBody(true, true);
    }
  }

  takeLifeBonus(obj1, obj2){
    let player=obj1;
    let lifeBonus=obj2;
    lifeBonus.disableBody(true, true);
    player.setLife(player.getLife()+1);
  }

  update() {
    this.tileSprite.tilePositionX+=5;
    this.rockLauncher.update();
    this.ground8Launcher.update();
    this.lifeBonusLauncher.update();
    this.lifeText.setText("Life: "+this.pumpkinman.getLife());
    if(this.pumpkinman.getLife()>0){
      this.timeText.setText(""+((new Date().getTime()-this.startTime)/1000));
    }
    Phaser.Actions.Call(this.platforms2.getChildren(), function(go) {
      go.setVelocityX(-50);
    });
    if (this.cursors.up.isDown && this.pumpkinman.body.touching.down) {
      this.pumpkinman.anims.play('jump');
      this.pumpkinman.setVelocityY(-500);
    } else {
      if (this.pumpkinman.body.touching.down) {
        if(this.pumpkinman.x<200){
          this.pumpkinman.setVelocityX(50);
        }else{
          this.pumpkinman.setVelocityX(0);
        }
        this.pumpkinman.anims.play('run', true);
      }
    }
  }
}

export { Game };
